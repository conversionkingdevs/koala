console.clear();

function defer(method, selector) {
    if (jQuery(selector).length > 0){
        method();
    } else {
        setTimeout(function() { defer(method, selector); }, 50);
    }     
}

function getScript(src, callback) {
    var s = document.createElement('script');
    s.src = src;
    s.async = true;
    s.onreadystatechange = s.onload = function() {
        if (!callback.done && (!s.readyState || /loaded|complete/.test(s.readyState))) {
            callback.done = true;
            callback();
        }
    };
    document.querySelector('head').appendChild(s);
};



getScript('https://code.jquery.com/jquery-2.2.4.min.js', function () {

    $ = jQuery.noConflict();
    window.slider = ($(window).width() < 770) ? $('.product__slider.mobile') : $('.product__slider:not(.mobile)');
    window.is_mobile = ($(window).width() < 770) ? true : false;
    window.interval;
    clearInterval(window.interval);
    window.control_nav = slider.find('.slider__dots');
    window.current_slide = 0;
    window.autoplay_timeout = 2000;
    window.autoplay_button = $('<span class="autoplay-control" />');
    window.slides = slider.find('slides_slide');

    function SliderControls (){
        console.log("SliderControl");
        if(is_mobile){
            control_nav.parent().append( autoplay_button );
        } else {
            control_nav.append( autoplay_button );
        }
        
        after_slide_change = function(e){
            setTimeout(function(){
                current_slide = get_active_slide();
            },10)
        }
        slider[0].addEventListener('after.lory.slide', after_slide_change);
        

        get_active_slide = function(){
            return control_nav.find('span.active').parent().index();        
        }


        control_nav.find(' > div').click(function(e){
            if ( e.originalEvent !== undefined ) {
                stop_autoplay();
            }
        })

        autoplay = function(){
            interval = setInterval(function(){
                next_slide();
            }, autoplay_timeout)
        }

        next_slide = function(){
            animate_to = (current_slide == control_nav.find(' > div').length - 1) ? 0 : current_slide + 1;
            control_nav.find(' > div:eq('+ animate_to +') span').trigger('click'); 
        }

        previous_slide = function(){
            animate_to = (current_slide == 0) ? control_nav.find(' > div').length - 1 : current_slide - 1;
            control_nav.find(' > div:eq('+ animate_to +') span').trigger('click'); 
        }

        start_autoplay = function(){
            autoplay_button.removeClass('pause').addClass('play');
            autoplay();
        }

        stop_autoplay = function(){
            $('.autoplay-control').removeClass('play').addClass('pause');
            clearInterval(interval);
        }
        
        
        window.slides.hover(function () {
            if ($('.autoplay-control').hasClass('play')) {
                stop_autoplay();
            }
        }, function () {
            if ($('.autoplay-control').hasClass('pause')) {
                start_autoplay();
            }
        })

        autoplay_button.click(function(){
            if( $(this).hasClass('pause') ){
                start_autoplay();
            } else {
                stop_autoplay();
            }
        })

        start_autoplay();

        slider[0].addEventListener('on.lory.touchend',stop_autoplay);

        $('head').append(`
        <style>
            .product__slider .autoplay-control {background-color: #224c69; background-position: center center; background-repeat: no-repeat; display: inline-block; border-radius: 4px; height: 36px; width: 35px; -webkit-box-shadow: 0 2px 5px rgba(0,0,0,.16); box-shadow: 0 2px 5px rgba(0,0,0,.16); margin-right: 15px; cursor:pointer; }
            .product__slider .autoplay-control.play {background-image: url(https://i.imgur.com/UNpWv5k.png);}
            .product__slider .autoplay-control.pause {background-image: url(https://i.imgur.com/bp76W23.png);}
            .product__slider .slider__dots div {cursor:pointer;}

            @media screen and (max-width:769px){
                .js_slider {position:relative;}
                .product__slider.mobile .autoplay-control {width:29px; height:28px; position:absolute; bottom:2.2vh; right:0; }            
            }

            @media screen and (max-width:420px){
                .product__slider.mobile .autoplay-control {bottom:2.2vh; right:auto; left:2.5vw; }            
            }
        </style>
        `);
    }

    SliderControls();

    (function(history){
        var pushState = history.pushState;
        history.pushState = function(state) {
            if (typeof history.onpushstate == "function") {
                history.onpushstate({state: state});
            }
            return pushState.apply(history, arguments);
        };
    })(window.history);
    
    window.onpopstate = history.onpushstate = function(e) {
        console.log("pushstate");
        setTimeout(function () {
            var URL = window.location.pathname;
            defer(function () {
                if (URL.indexOf("/products/koala-mattress") >= 1 || 
                    URL.indexOf("/products/koala-timber-bed-base") >= 1 || 
                    URL.indexOf("/products/koala-summer-sheets") >= 1 ||
                    URL.indexOf("/products/koala-sofa" >= 1)) {
                        console.log('Timeout');
                        window.slider = ($(window).width() < 770) ? $('.product__slider.mobile') : $('.product__slider:not(.mobile)');
                        window.is_mobile = ($(window).width() < 770) ? true : false;
                        window.interval;
                        clearInterval(window.interval);
                        window.control_nav = slider.find('.slider__dots');
                        window.current_slide = 0;
                        window.autoplay_timeout = 2000;
                        window.autoplay_button = $('<span class="autoplay-control" />');
                        SliderControls();
                }
            }, '.product__slider');
        },1000);  
    }
});

