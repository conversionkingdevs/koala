console.clear();

function defer(method, selector) {
    if (jQuery(selector).length > 0){
        method();
    } else {
        setTimeout(function() { defer(method, selector); }, 50);
    }     
}

function getScript(src, callback) {
    var s = document.createElement('script');
    s.src = src;
    s.async = true;
    s.onreadystatechange = s.onload = function() {
        if (!callback.done && (!s.readyState || /loaded|complete/.test(s.readyState))) {
            callback.done = true;
            callback();
        }
    };
    document.querySelector('head').appendChild(s);
};



getScript('https://code.jquery.com/jquery-2.2.4.min.js', function () {
        $ = jQuery.noConflict();
        window.interval;
        clearInterval(window.interval);
        window.is_mobile = ($(window).width() < 770) ? true : false;
        window.slider = (window.is_mobile) ? $('.product__slider.mobile') : $('.product__slider:not(.mobile)');
        window.control_nav = window.slider.find('.slider__dots');
        window.current_slide = 0;
        window.autoplay_timeout = 2000;

        function SliderControls () {
        control_nav.append('<a href="#" class="slider-next" />');
        control_nav.append('<a href="#" class="slider-prev" />');

        if(is_mobile){
            control_nav.find(' > div').each(function(){
                $(this).find('span').attr('style', $(this).find('span').attr('style').replace(';',' !important;') );
            })
        }

        
            
        after_slide_change = function(e){
            setTimeout(function(){
                current_slide = get_active_slide();
            },10)
        }
        slider[0].addEventListener('after.lory.slide', after_slide_change);
        

        get_active_slide = function(){
            return control_nav.find('span.active').parent().index();        
        }


        control_nav.find(' > div').click(function(e){
            if ( e.originalEvent !== undefined ) {
                stop_autoplay();
            }
        })

        control_nav.find('a.slider-next').click(function(e){
            e.preventDefault();
            stop_autoplay();
            next_slide();
        })

        control_nav.find('a.slider-prev').click(function(e){
            e.preventDefault();
            stop_autoplay();
            previous_slide();
        })

        autoplay = function(){
            interval = setInterval(function(){
                next_slide();
            }, autoplay_timeout)
        }

        next_slide = function(){
            animate_to = (current_slide == control_nav.find(' > div').length - 1) ? 0 : current_slide + 1;
            control_nav.find(' > div:eq('+ animate_to +') span').trigger('click'); 
        }

        previous_slide = function(){
            animate_to = (current_slide == 0) ? control_nav.find(' > div').length - 1 : current_slide - 1;
            control_nav.find(' > div:eq('+ animate_to +') span').trigger('click'); 
        }

        start_autoplay = function(){
            autoplay();
        }

        stop_autoplay = function(){        
            clearInterval(interval);
        }

        start_autoplay();

        slider[0].addEventListener('on.lory.touchend', stop_autoplay);

        $('head').append(`
        <style>
            .js_slider {position:relative;}
            .product__slider .slider__dots {margin:0; bottom:-15px; left:50%; box-shadow:0 2px 5px rgba(0,0,0,.16); background:#fff; padding:9px 18px 9px 38px; border-radius:3px;}
            .product__slider .slider__dots div {cursor:pointer; box-shadow:none;}
            .product__slider .slider__dots div span {margin-right:20px;}

            .product__slider .slider__dots .slider-next {width:9px; height:18px; position:absolute; top:50%; margin-top:-9px; right:10px; background:transparent url(https://i.imgur.com/f4KT265.png) no-repeat center center;}
            .product__slider .slider__dots .slider-prev {width:9px; height:18px; position:absolute; top:50%; margin-top:-9px; left:10px;  background:transparent url(https://i.imgur.com/07ZyDvm.png) no-repeat center center;}
            
            @media screen and (max-width:769px){
                .product__slider .slider__dots {right:auto;}
                .product__slider .slider__dots div span { width:36px; height:36px; border-radius:3px; background-size:cover !important;}
            
            }

            @media screen and (max-width:420px){
            
            }
        </style>
        `);

        setTimeout(function(){
            control_nav.css('margin-left', control_nav.outerWidth() / -2);
        },50)
        control_nav.css('margin-left', control_nav.outerWidth() / -2);
    }
    
    SliderControls();

    (function(history){
        var pushState = history.pushState;
        history.pushState = function(state) {
            if (typeof history.onpushstate == "function") {
                history.onpushstate({state: state});
            }
            return pushState.apply(history, arguments);
        };
    })(window.history);
    
    window.onpopstate = history.onpushstate = function(e) {
        console.log("pushstate");
        setTimeout(function () {
            var URL = window.location.pathname;
                if (URL.indexOf("/products/koala-mattress") >= 1 || 
                    URL.indexOf("/products/koala-timber-bed-base") >= 1 || 
                    URL.indexOf("/products/koala-summer-sheets") >= 1 ||
                    URL.indexOf("/products/koala-sofa" >= 1)) {
                        //CODE HERE
                }
        },1000);  
    }
});
