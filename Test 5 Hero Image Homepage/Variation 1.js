function getScript(src, callback) {
    var s = document.createElement('script');
    s.src = src;
    s.async = true;
    s.onreadystatechange = s.onload = function() {
        if (!callback.done && (!s.readyState || /loaded|complete/.test(s.readyState))) {
            callback.done = true;
            callback();
        }
    };
    document.querySelector('head').appendChild(s);
};


getScript('https://code.jquery.com/jquery-2.2.4.min.js', function () {
    function start () {
        console.log('Test 5 Running')
        vwo_$('body').removeClass('opt5');
        $('body').removeClass('opt5-view');
        $('.opt5-hero').remove();
        if (!$('body').hasClass('opt5-view') && !$('body').hasClass('opt5') ) {
            $('body').addClass('opt5');
            $('.hero').after('<section class="hero opt5-hero" data-v-7688e0e9="" data-v-175cb36d=""><div class="container" data-v-7688e0e9=""><h2 data-v-7688e0e9="">australia\'s highest rated<br> mattress brand</h2><a href="/products/koala-sofa" class="btn btn--dark-green" data-v-7688e0e9="">Shop The Sofa          </a><a class="opt5-link" href="https://au.koala.com/products/koala-mattress">shop the mattress</a></div></section>');
            $('body').addClass('opt5-view');
        }
    }

    start();

    (function(history){
        var pushState = history.pushState;
        history.pushState = function(state) {
            if (typeof history.onpushstate == "function") {
                history.onpushstate({state: state});
            }
            return pushState.apply(history, arguments);
        };
    })(window.history);
        
    window.onpopstate = history.onpushstate = function(e) {
        console.log("pushstate");
        setTimeout(function () {
            var URL = window.location.origin + window.location.pathname;
            if (URL == "https://au.koala.com/") {
                start();
            }
        },1000); 
    }
});
    